﻿using Repository;

using Services.Interfaces;
using Services.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CompanyWeb.Controllers
{
   
    public class HomeController : Controller
    {
        private readonly IReportService _reportService;
        private readonly ICategoryService _categoryService;
        public HomeController(IReportService reportService, ICategoryService categoryService)
        {
             _reportService = reportService;
             _categoryService = categoryService;
        }
        public ActionResult Index()
        {
            var x = _reportService.GetCustomerCategories(1);
            var z = _categoryService.GetAll().ToList();
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}