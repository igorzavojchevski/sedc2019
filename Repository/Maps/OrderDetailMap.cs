﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Maps
{
   public class OrderDetailMap:EntityTypeConfiguration<OrderDetail>
    {
        public OrderDetailMap()
        {
            this.Ignore(t => t.CreatedBy);
            this.Ignore(t => t.CreatedOn);
            this.Ignore(t => t.LastModifiedOn);
            this.Ignore(t => t.LastModifiedBy);

            HasKey(t => new { t.OrderID, t.ProductID });
        
            Property(e => e.UnitPrice)
             .HasPrecision(19, 2);

            Property(e => e.Discount)
                .HasPrecision(19, 2);
        }
    }
}
